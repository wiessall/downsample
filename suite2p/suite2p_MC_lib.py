from suite2p import registration, default_ops
import shutil
import numpy as np
import time
import os
from tifffile import imread, imwrite, TiffFile

def format_suite2p_normal(filename):
    tif = TiffFile(filename)
    tif = imread(filename, key=range(len(tif.pages)))
    imwrite(filename, tif)

#%%
def pipeline(f_reg, f_raw=None, f_reg_chan2=None, f_raw_chan2=None,
             run_registration=True, ops=default_ops(), stat=None):
    if run_registration:
        plane_times = {}
        t1 = time.time()




        raw = f_raw is not None
        # if already shifted by bidiphase, do not shift again
        if not raw and ops['do_bidiphase'] and ops['bidiphase'] != 0:
            ops['bidi_corrected'] = True

        ######### REGISTRATION #########
        t11 = time.time()
        print('----------- REGISTRATION')
        refImg = ops['refImg'] if 'refImg' in ops and ops.get('force_refImg', False) else None

        align_by_chan2 = ops['functional_chan'] != ops['align_by_chan']
        registration_outputs = registration.register.registration_wrapper(f_reg, f_raw=f_raw, f_reg_chan2=f_reg_chan2,
                                                                          f_raw_chan2=f_raw_chan2,
                                                                          refImg=refImg, align_by_chan2=align_by_chan2,
                                                                          ops=ops)

        ops = registration.register.save_registration_outputs_to_ops(registration_outputs, ops)
        # add enhanced mean image
        meanImgE = registration.register.compute_enhanced_mean_image(ops['meanImg'].astype(np.float32), ops)
        ops['meanImgE'] = meanImgE

        if ops.get('ops_path'):
            np.save(ops['ops_path'], ops)

        plane_times['registration'] = time.time() - t11
        print('----------- Total %0.2f sec' % plane_times['registration'])
        n_frames, Ly, Lx = f_reg.shape

        if ops['two_step_registration'] and ops['keep_movie_raw']:
            print('----------- REGISTRATION STEP 2')
            print('(making mean image (excluding bad frames)')
            nsamps = min(n_frames, 1000)
            inds = np.linspace(0, n_frames, 1 + nsamps).astype(np.int64)[:-1]
            if align_by_chan2:
                refImg = f_reg_chan2[inds].astype(np.float32).mean(axis=0)
            else:
                refImg = f_reg[inds].astype(np.float32).mean(axis=0)
            registration_outputs = registration.register.registration_wrapper(f_reg, f_raw=None, f_reg_chan2=f_reg_chan2,
                                                                              f_raw_chan2=None,
                                                                              refImg=refImg, align_by_chan2=align_by_chan2,
                                                                              ops=ops)
            if ops.get('ops_path'):
                np.save(ops['ops_path'], ops)
            plane_times['two_step_registration'] = time.time() - t11
            print('----------- Total %0.2f sec' % plane_times['two_step_registration'])

        # compute metrics for registration
        if ops.get('do_regmetrics', True) and n_frames >= 1500:
            t0 = time.time()
            # n frames to pick from full movie
            nsamp = min(2000 if n_frames < 5000 or Ly > 700 or Lx > 700 else 5000, n_frames)
            inds = np.linspace(0, n_frames - 1, nsamp).astype('int')
            mov = f_reg[inds]
            mov = mov[:, ops['yrange'][0]:ops['yrange'][-1], ops['xrange'][0]:ops['xrange'][-1]]
            ops = registration.get_pc_metrics(mov, ops)
            plane_times['registration_metrics'] = time.time() - t0
            print('Registration metrics, %0.2f sec.' % plane_times['registration_metrics'])
            if ops.get('ops_path'):
                np.save(ops['ops_path'], ops)
        ###########
        if ops.get('move_bin') and ops['save_path'] != ops['fast_disk']:
            print('moving binary files to save_path')
            shutil.move(ops['reg_file'], os.path.join(ops['save_path'], 'data.bin'))
            if ops['nchannels'] > 1:
                shutil.move(ops['reg_file_chan2'], os.path.join(ops['save_path'], 'data_chan2.bin'))
            if 'raw_file' in ops:
                shutil.move(ops['raw_file'], os.path.join(ops['save_path'], 'data_raw.bin'))
                if ops['nchannels'] > 1:
                    shutil.move(ops['raw_file_chan2'], os.path.join(ops['save_path'], 'data_chan2_raw.bin'))
        elif ops.get('delete_bin'):
            print('deleting binary files')
            os.remove(ops['reg_file'])
            if ops['nchannels'] > 1:
                os.remove(ops['reg_file_chan2'])
            if 'raw_file' in ops:
                os.remove(ops['raw_file'])
                if ops['nchannels'] > 1:
                    os.remove(ops['raw_file_chan2'])
    return ops
