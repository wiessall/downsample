import numpy as np
import sys
from tifffile import imsave, imwrite, imread, TiffFile


def format_suite2p_normal(filename):
    tif = TiffFile(filename)
    tif = imread(filename, key=range(len(tif.pages)))
    imwrite(filename, tif)


def split_stack(stack_data):
    """
    Split the stack data into sub-stacks smaller than 4GB to handle better the
    resulting .tif files.

    Parameters
    ----------
    stack_data : np.array(uint(16))
    np array of calcium data. Shape (channels, z, x, y)


    Returns
    -------
    stack_data : np.array(uint(16))
    nested array of the original data, the single parts being smaller than
    4GB.

    """

    stack_data = stack_data.squeeze()

    stack_splits = 10

    stack_length = stack_data.shape[0]

    stack_split_idcs = np.linspace(0, stack_length, num=stack_splits, dtype=int)[1:]

    stack_data = np.split(stack_data, stack_split_idcs)

    return stack_data
