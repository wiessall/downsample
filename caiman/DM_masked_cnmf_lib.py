from __future__ import division
from __future__ import print_function
from builtins import zip
from builtins import str
from builtins import map
from builtins import range


# introduced by jaime
import warnings

# warnings.filterwarnings("ignore", message="numpy.dtype size changed")
# warnings.filterwarnings("ignore", message="numpy.ufunc size changed")


import glob
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import time
import datetime

# import cv2
# try:
#    cv2.setNumThreads(0)
# except:
#    pass

try:
    if __IPYTHON__:
        print((1))
        # this is used for debugging purposes only. allows to reload classes
        # when changed
        get_ipython().magic("load_ext autoreload")
        get_ipython().magic("autoreload 2")
except NameError:
    print("Not IPYTHON")
    pass

import caiman as cm
from caiman.utils.utils import download_demo
from caiman.utils.visualization import (
    plot_contours,
    nb_view_patches,
    nb_plot_contour,
    get_contours,
)
import pylab as pl
from caiman.base.rois import com
from caiman.source_extraction.cnmf import cnmf as cnmf
from caiman.motion_correction import MotionCorrect
from caiman.source_extraction.cnmf.utilities import detrend_df_f
from caiman.components_evaluation import estimate_components_quality_auto
import bokeh.plotting as bpl

bpl.output_notebook()


# First, define function wich includes complete masked cnmf pipeline


def masked_cnmf(list_of_files, path_to_masks, results_index):
    ### BLOCK-1

    # Parameters
    fname = list_of_files
    fr = 15.4  # imaging rate in frames per second
    decay_time = 1.0  # 1 second for GCaMP6s, approximate length of a typical transient in seconds
    # motion correction parameters are the same as for mask generation
    niter_rig = 1  # number of iterations for rigid motion correction
    max_shifts = (2, 2)  # maximum allow rigid shift
    splits_rig = (
        75  # for parallelization split the movies in  num_splits chuncks across time
    )
    strides = (
        114,
        114,
    )  # start a new patch for pw-rigid motion correction every x pixels
    overlaps = (30, 30)  # overlap between pathes (size of patch strides+overlaps)
    splits_els = (
        75  # for parallelization split the movies in  num_splits chuncks across time
    )
    upsample_factor_grid = 2  # upsample factor to avoid smearing when merging patches
    max_deviation_rigid = (
        2  # maximum deviation allowed for patch with respect to rigid shifts
    )
    # parameters for source extraction and deconvolution
    p = 1  # order of the autoregressive system, when 0 deconvolution is turned off
    gnb = 2  # number of global background components
    merge_thresh = 0.8  # merging threshold, max correlation allowed
    rf = None  # half-size of the patches in pixels. e.g., if rf=25, patches are 50x50
    stride_cnmf = None  # amount of overlap between the patches in pixels
    K = 80  # it will be taken from the mask
    gSig = [10, 10]  # expected half size of neurons
    init_method = "sparse_nmf"  # initialization method (if analyzing dendritic data using 'sparse_nmf')
    is_dendrites = True  # flag for analyzing dendritic data
    alpha_snmf = 0.8  # it was 0.8, sparsity penalty for dendritic data analysis through sparse NMF
    # parameters for component evaluation
    min_SNR = 3.0  # signal to noise ratio for accepting a component
    rval_thr = 0.4  # it was 0.4, space correlation threshold for accepting a component
    cnn_thr = 0.4  # threshold for CNN based classifier

    ### BLOCK-2

    # masks upload and selection
    A_filepath = path_to_masks
    print(datetime.datetime.now(), " Loading: ", A_filepath)
    A_small_file = np.load(A_filepath)  # File load

    # Check if iterative mask generation failed
    shape_condition_1 = A_small_file.shape[0] == 1
    shape_condition_2 = A_small_file.shape[1] == 1
    value_conditon = np.all(A_small_file is np.nan)
    if shape_condition_1 and shape_condition_2 and value_condition:
        p1 = os.path.split(fname_new)[0]
        p2 = results_index + ".npz"
        print(p1)
        print(p2)
        np.savez(os.path.join(p1, p2), THIS_K_FAILED=np.full((1, 1), np.nan))
        return False

    Ain_small_file = np.greater(
        A_small_file, np.zeros(A_small_file.shape)
    )  # Mask binarization
    Ain_masks = Ain_small_file[:, :]
    print(Ain_masks.shape)
    ### BLOCK-3

    # %% start a cluster for parallel processing
    c, dview, n_processes = cm.cluster.setup_cluster(
        backend="local", n_processes=None, single_thread=False
    )
    # first we create a motion correction object with the parameters specified
    min_mov = min(
        cm.load(fname[:], subindices=range(200)).min(), 0
    )  # cm.load(fname[0],
    mc = MotionCorrect(
        fname[:],  # mc = MotionCorrect(fname[0],
        min_mov,
        dview=dview,
        max_shifts=max_shifts,
        niter_rig=niter_rig,
        splits_rig=splits_rig,
        strides=strides,
        overlaps=overlaps,
        splits_els=splits_els,
        upsample_factor_grid=upsample_factor_grid,
        max_deviation_rigid=max_deviation_rigid,
        shifts_opencv=True,
        nonneg_movie=True,
    )
    print(datetime.datetime.now(), " starting motion correction")
    # %% Run piecewise-rigid motion correction using NoRMCorre
    mc.motion_correct_pwrigid(save_movie=True)  # Perform motion correction
    print(datetime.datetime.now(), " finished motion correction")
    print(mc.fname_tot_els)  # Print list of MMAP file(s)
    m_els = cm.load(mc.fname_tot_els)  # Load MMAP file(s) to memory
    bord_px_els = np.ceil(
        np.maximum(np.max(np.abs(mc.x_shifts_els)), np.max(np.abs(mc.y_shifts_els)))
    ).astype(np.int)

    ### BLOCK-4

    # %% MEMORY MAPPING
    # memory map the file in order 'C'
    fnames = mc.fname_tot_els  # name of the pw-rigidly corrected file.
    border_to_0 = bord_px_els  # number of pixels to exclude
    fname_new = cm.save_memmap(
        fnames, base_name="memmap_", order="C", border_to_0=bord_px_els
    )  # exclude borders

    # now load the file
    Yr, dims, T = cm.load_memmap(fname_new)
    d1, d2 = dims
    images = np.reshape(Yr.T, [T] + list(dims), order="F")
    # load frames in python format (T x X x Y)

    # %% restart cluster to clean up memory
    # cm.stop_server(dview=dview)
    dview.terminate()
    c, dview, n_processes = cm.cluster.setup_cluster(
        backend="local", n_processes=None, single_thread=False
    )

    ### BLOCK-5

    # %% Run CNMF with master mask
    # First extract spatial and temporal components on patches and combine them
    # for this step deconvolution is turned off (p=0)
    t1 = time.time()
    print(datetime.datetime.now(), " starting CNMF")
    print(n_processes)
    from caiman.source_extraction.cnmf import params as params

    # Set parameteres for mask-based CNMF
    opts_dict = {
        "fnames": fname,
        "fr": fr,
        "decay_time": decay_time,
        "strides": strides,
        "overlaps": overlaps,
        "max_shifts": max_shifts,
        "max_deviation_rigid": max_deviation_rigid,
        "p": 0,
        "gnb": gnb,
        "rf": None,  # rf
        "K": None,  # K
        "gSig": None,  # gSig
        "stride": None,  # stride_cnmf
        "rolling_sum": True,
        "only_init": False,  # True
        "merge_thr": 0.9999,  # merge_thresh    # do not use to 'None', will fail to merge component
    }
    opts = params.CNMFParams(params_dict=opts_dict)
    rf = None  # half-size of the patches in pixels. Should be `None` when seeded CNMF is used.
    only_init = False  # has to be `False` when seeded CNMF is used
    # params object
    opts_dict = {
        "rf": rf,
        "only_init": only_init,
    }
    opts.change_params(opts_dict)
    cnm = cnmf.CNMF(n_processes, params=opts, dview=dview, Ain=Ain_masks)
    cnm = cnm.fit(images)

    ### BLOCK-7
    print(datetime.datetime.now(), " CNMF done")
    print(cnm)
    # %% plot contours of found components
    Cn = cm.local_correlations(images.transpose(1, 2, 0))
    Cn[np.isnan(Cn)] = 0
    #    plt.figure()
    #    crd = plot_contours(cnm.estimates.A, Cn, thr=0.99)
    #    plt.title('Contour plots of found components')

    # %% COMPONENT EVALUATION
    # the components are evaluated in three ways:
    #   a) the shape of each component must be correlated with the data
    #   b) a minimum peak SNR is required over the length of a transient
    min_SNR = 3.0  # signal to noise ratio for accepting a component
    rval_thr = 0.4  # it was 0.4, space correlation threshold for accepting a component
    cnn_thr = 0.4  # threshold for CNN based classifier
    (
        idx_components,
        idx_components_bad,
        SNR_comp,
        r_values,
        cnn_preds,
    ) = estimate_components_quality_auto(
        images,
        cnm.estimates.A,
        cnm.estimates.C,
        cnm.estimates.b,
        cnm.estimates.f,
        cnm.estimates.YrA,
        fr,
        decay_time,
        gSig,
        dims,
        dview=dview,
        min_SNR=min_SNR,
        r_values_min=rval_thr,
        use_cnn=False,
        thresh_cnn_lowest=cnn_thr,
    )

    # Good and bad components plotting
    #    plt.figure();
    #    plt.subplot(121)
    #    crd_good = cm.utils.visualization.plot_contours(cnm.estimates.A[:,idx_components], Cn, thr=.8, vmax=0.75)
    #    plt.title('Contour plots of accepted components')
    #    plt.subplot(122)
    #    crd_bad = cm.utils.visualization.plot_contours(cnm.estimates.A[:,idx_components_bad], Cn, thr=.8, vmax=0.75)
    #    plt.title('Contour plots of rejected components')

    ### BLOCK-8

    # %% RE-RUN seeded CNMF on accepted patches to refine and perform deconvolution
    A_in = cnm.estimates.A[:, idx_components]
    C_in = cnm.estimates.C[idx_components]
    b_in = cnm.estimates.b
    f_in = cnm.estimates.f
    cnm2 = cnmf.CNMF(
        n_processes=n_processes,
        k=A_in.shape[-1],
        gSig=gSig,
        p=1,  # It is here for deconvolution
        dview=dview,
        merge_thresh=0.999,  # merge_thresh,
        Ain=A_in,
        Cin=C_in,
        b_in=b_in,
        f_in=f_in,
        rf=None,
        stride=None,
        gnb=gnb,
        method_deconvolution="oasis",
        check_nan=True,
    )
    cnm2 = cnm2.fit(images)

    # %% Extract DF/F values
    F_dff = detrend_df_f(
        cnm2.estimates.A,
        cnm2.estimates.b,
        cnm2.estimates.C,
        cnm2.estimates.f,
        YrA=cnm2.estimates.YrA,
        quantileMin=8,
        frames_window=250,
    )

    ### BLOCK-9

    # Results saving from CNM2
    save_results = True
    if save_results:
        p1 = os.path.split(fname_new)[0]
        p2 = results_index + ".npz"
        print(p1)
        print(p2)
        np.savez(
            os.path.join(p1, p2),
            S=cnm2.estimates.S,  # added 12.9.2019
            F_dff=F_dff,  # added 12.9.2019
            Cn=Cn,
            A=cnm2.estimates.A.todense(),
            C=cnm2.estimates.C,
            b=cnm2.estimates.b,
            f=cnm2.estimates.f,
            YrA=cnm2.estimates.YrA,
            sn=1,
            d1=d1,
            d2=d2,
            idx_components=idx_components,
            idx_components_bad=idx_components_bad,
        )

    ### BLOCK-10

    # %% STOP CLUSTER and clean up log files
    cm.stop_server(dview=dview)
    log_files = glob.glob("*_LOG_*")
    for log_file in log_files:
        os.remove(log_file)


# %% Get complete file names with the path from pointed folder
from os import listdir


def get_files(dirpath):
    filelist = listdir(dirpath)
    fname = []
    for i in range(len(filelist)):
        filename = filelist[i]
        filepath = dirpath + "/" + filename
        fname.append(filepath)
    return fname
