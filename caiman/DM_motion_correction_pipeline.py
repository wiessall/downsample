from __future__ import division
from __future__ import print_function
from builtins import zip
from builtins import str
from builtins import map
from builtins import range


# introduced by jaime
import warnings

warnings.filterwarnings("ignore")
# warnings.filterwarnings("ignore", message="numpy.ufunc size changed")


import glob
import numpy as np
import os
import sys
import time
from tifffile import imsave, imwrite, imread, TiffFile
from natsort import natsorted
from DM_motion_correction_lib import split_stack, format_suite2p_normal

import caiman as cm
from caiman.utils.utils import download_demo
from caiman.utils.visualization import plot_contours, nb_view_patches, nb_plot_contour
from caiman.source_extraction.cnmf import cnmf as cnmf
from caiman.motion_correction import MotionCorrect
from caiman.source_extraction.cnmf.utilities import detrend_df_f
from caiman.components_evaluation import estimate_components_quality_auto
import bokeh.plotting as bpl

bpl.output_notebook()

import argparse


parser = argparse.ArgumentParser()
# set start and end time
parser.add_argument(
    "-i",
    "--input",
    type=str,
    default=".",
    help="input folder, takes tif files from there",
)

parser.add_argument("-o", "--output", type=str, default="to_ana", help="output folder")
args = parser.parse_args()

save_folder = args.output
os.makedirs(save_folder, exist_ok=True)

# Motion correction only block
# For rigid-corrected images line 118 cuts them from 512 x 512 to 456 x 456

# dataset dependent parameters
wdir = args.input
fname = natsorted(glob.glob(os.path.join(wdir, "*tif")))

# The files generated here must not be shorter than 200 frames, caimans movie
# is expecting longer files

for number, f in enumerate(fname):
    format_suite2p_normal(f)
    if len(TiffFile(f).pages) < 499:
        current_tif = imread(f)
        os.remove(f)
        last_f = fname[number - 1]
        last_tif = imread(last_f)
        current_tif = np.concatenate([last_tif, current_tif])
        imwrite(last_f, current_tif)

fname = natsorted(glob.glob(os.path.join(wdir, "*tif")))
fr = 15.4  # imaging rate in frames per second
decay_time = 1.0  # length of a typical transient in seconds

# motion correction parameters
niter_rig = 10  # (it was 3) number of iterations for rigid motion correction
max_shifts = (15, 15)  # (it was 7, 7) maximum allow rigid shift
splits_rig = 75  # (it was 56) for parallelization split the movies in  num_splits chuncks across time
strides = (60, 60)  # start a new patch for pw-rigid motion correction every x pixels
overlaps = (
    20,
    20,
)  # (it was 24) overlap between pathes (size of patch strides+overlaps)
splits_els = 75  # (it was 56) for parallelization split the movies in  num_splits chuncks across time
upsample_factor_grid = 4  # upsample factor to avoid smearing when merging patches
max_deviation_rigid = (
    15  # (it was 3) maximum deviation allowed for patch with respect to rigid shifts
)

# %% start a cluster for parallel processing
# dview.terminate()
c, dview, n_processes = cm.cluster.setup_cluster(
    backend="local", n_processes=None, single_thread=False
)

# first we create a motion correction object with the parameters specified
min_mov = min(cm.load(fname[:], subindices=range(200)).min(), 0)  # cm.load(fname[0],
# this will be subtracted from the movie to make it non-negative

mc = MotionCorrect(
    fname[:],  # mc = MotionCorrect(fname[0],
    min_mov,
    dview=dview,
    max_shifts=max_shifts,
    niter_rig=niter_rig,
    splits_rig=splits_rig,
    strides=strides,
    overlaps=overlaps,
    splits_els=splits_els,
    upsample_factor_grid=upsample_factor_grid,
    max_deviation_rigid=max_deviation_rigid,
    shifts_opencv=True,
    nonneg_movie=True
    # , pw_rigid=pw_rigid
)
# note that the file is not loaded in memory
# %%capture
# %% Run piecewise-rigid motion correction using NoRMCorre
mc.motion_correct_pwrigid(save_movie=True)  # Perform motion correction
print(mc.fname_tot_els)  # Print list of MMAP file(s)
m_els = cm.load(mc.fname_tot_els)  # Load MMAP file(s) to memory
bord_px_els = np.ceil(
    np.maximum(np.max(np.abs(mc.x_shifts_els)), np.max(np.abs(mc.y_shifts_els)))
).astype(np.int)
# maximum shift to be used for trimming against NaNs
##New 2022-06-08
m_els = m_els[:, 28:-28, 28:-28].astype(np.int16)
####STACK SPLIT
m_els = split_stack(m_els)
m_els = [element for element in m_els if element.shape[0]]
for i, substack in enumerate(m_els):
    savepath = os.path.join(save_folder, str(i) + "_NormCorre_MC.tif")
    print(savepath)
    imsave(savepath, substack, bigtiff=False)

# %% STOP CLUSTER and clean up log files
cm.stop_server(dview=dview)
log_files = glob.glob("*_LOG_*")
for log_file in log_files:
    os.remove(log_file)

dview.terminate()
