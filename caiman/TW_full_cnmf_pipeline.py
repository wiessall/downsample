

# Cell
# import bokeh.plotting as bpl
import argparse
import logging
import numpy as np
import os
import glob

from utils import set_caiman_parameters
from caiman.source_extraction.cnmf.utilities import detrend_df_f

import caiman as cm
from caiman.source_extraction.cnmf import cnmf as cnmf
from caiman.source_extraction.cnmf import params as params
from caiman.source_extraction.cnmf.utilities import detrend_df_f



def main(args):

     """Run caiman cnmf using provided commandline arguments."""
     mmap_path = glob.glob(os.path.join(args.fname_new, "*mmap"))[-1]
     Yr, dims, T = cm.load_memmap(mmap_path)
     d1, d2 = dims
     images = np.reshape(Yr.T, [T] + list(dims), order='F')

     caiman_opts_dict = set_caiman_parameters(args.parameters)

     opts = params.CNMFParams(params_dict=caiman_opts_dict)

     CNMF_params = opts.get_group('motion')
     print("CNMF_params:", CNMF_params)

     #%% RUN CNMF ON PATCHES

     # First extract spatial and temporal components on patches and combine them
     # for this step deconvolution is turned off (p=0)
     print("Starting CNMF ROI Selection algorithm")

     # start a cluster for parallel processing
     c, dview, n_processes = cm.cluster.setup_cluster(
          backend="local", n_processes=None, single_thread=False
      )
      
     cnm = cnmf.CNMF(n_processes, params=opts, dview=dview, Ain=None)

     cnm = cnm.fit(images)

     Cn = cm.local_correlations(images.transpose(1,2,0))
     Cn[np.isnan(Cn)] = 0

     print("Extract DF/F")
     #%% Extract DF/F values
     quantileMin         = caiman_opts_dict['quantileMin']
     frames_window       =  caiman_opts_dict['frames_window']
     F_dff = detrend_df_f(
         cnm.estimates.A,
         cnm.estimates.b,
         cnm.estimates.C,
         cnm.estimates.f,
         cnm.estimates.YrA,
         quantileMin=quantileMin,
         frames_window=frames_window)

     cnm2 = None
     if caiman_opts_dict['refit']:
         print("Performing refit")
         cnm2 = cnm.refit(images, dview=dview)

         print("Extract DF/F")
         cnm2.estimates.detrend_df_f(quantileMin=quantileMin, frames_window=frames_window)

     if caiman_opts_dict['evaluate_results']:
         cnm.estimates.evaluate_components(images, cnm.params, dview=dview)
         if caiman_opts_dict['refit']:
             cnm2.estimates.evaluate_components(images, cnm2.params, dview=dview)
             cnm2.estimates.select_components(use_object=True)


     p1 = os.path.split(mmap_path)[0]
     p2 = "final_masks_analysis_full_cnmf.npz"
     print(p1)
     print(p2)
     np.savez(
           os.path.join(p1, p2),
           S=cnm2.estimates.S,  # added 12.9.2019
           F_dff=F_dff,  # added 12.9.2019
           Cn=Cn,
           A=cnm2.estimates.A.todense(),
           C=cnm2.estimates.C,
           b=cnm2.estimates.b,
           f=cnm2.estimates.f,
           YrA=cnm2.estimates.YrA,
           sn=1,
           d1=d1,
           d2=d2,
     )


     print ("Done!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # set start and end time
    parser.add_argument(
        "-f",
        "--fname-new",
        dest="fname_new",
        type=str,
        help="input memmap file",
    )

    parser.add_argument(
      "-p", "--parameters", type=str,
      help="caiman parameters (YAML file)"
    )
    args = parser.parse_args()
    
    main(args)

