from DM_iterative_mask_generation_lib import *
import warnings

warnings.filterwarnings("ignore")

import argparse

parser = argparse.ArgumentParser()
# set start and end time
parser.add_argument(
    "-i",
    "--input",
    type=str,
    default="input.tif",
    help="input path, takes 456x456 tif files only",
)
parser.add_argument(
    "-k",
    "--components_estimate",
    type=int,
    nargs="+",
    default=50,
    help="K parameter, estimated number of components in video",
)

args = parser.parse_args()


# Iterative masks generation (cycle)

# Full path to the imaging file for masks generation
# Use only water stimulus response for this: increases speed
wdir = args.input
# For the mask generation only the white noise responses are needed.
# Currently they are at the beginning of the recording
fname = sorted(glob.glob(os.path.join(wdir, "*tif")))[:2]
assert len(fname)>0, "no tif tiles in {}".format(wdir)

Ks = args.components_estimate  # Number of componets for CNMF initialization
for K in Ks:
    results_index = "K{}".format(str(K))  # Note for results file

    # Run the first round of masks generation
    initial_masks, fname_new = initialize_masks(fname, K)
    print("*** Initial masks generated ***")
    print("{} input saved to {} output".format(fname, fname_new))

    # Run the second round of masks generation
    intermediate_masks = intermediate_masked_cnmf(initial_masks, fname_new, fname)
    print("*** Intermediate masks generated ***")

    # Compare number of clusters between two rounds
    nr_of_clusters_1 = initial_masks.shape[1]
    nr_of_clusters_2 = intermediate_masks.shape[1]
    iteration = 1
    clusters = []
    clusters = np.append(
        clusters, (initial_masks.shape[1], intermediate_masks.shape[1])
    )

    while nr_of_clusters_1 != nr_of_clusters_2:
        # Re-assign previous nr of clusters to nr_of_clusters_1
        nr_of_clusters_1 = intermediate_masks.shape[1]

        # Generate new intermediate masks
        intermediate_masks = intermediate_masked_cnmf(
            intermediate_masks, fname_new, fname
        )
        print("*** Intermediate masks generated ***")

        # Assign new nr of clusters to nr_of_clusters_2
        nr_of_clusters_2 = intermediate_masks.shape[1]

        # Count iterations
        iteration = iteration + 1
        clusters = np.append(clusters, intermediate_masks.shape[1])

    # Perform the final mask generation and save data
    print("*** Initializing generation of the final masks... ***")
    exitCode = last_masked_cnmf(fname_new, intermediate_masks, results_index, fname)
    if exitCode is False:
        print("*** INDEX ERROR K{} ***".format(K))
    else:
        p1 = os.path.split(fname_new)[0]
        open(f"{p1}/{K}_mask_done_flag", "x")

    print("*** FINAL MASKS GENERATED ***")
    print("Number of iterations: ", iteration)
    print("Number of clusters in each iteration: ", clusters)
