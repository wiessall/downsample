from __future__ import division
from __future__ import print_function
from builtins import zip
from builtins import str
from builtins import map
from builtins import range
from past.utils import old_div


# introduced by jaime
import warnings

# warnings.filterwarnings("ignore", message="numpy.dtype size changed")
# warnings.filterwarnings("ignore", message="numpy.ufunc size changed")


import glob
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import time

import caiman as cm
from caiman.utils.utils import download_demo
from caiman.utils.visualization import (
    plot_contours,
    nb_view_patches,
    nb_plot_contour,
    get_contours,
)
import pylab as pl
from caiman.base.rois import com
from caiman.source_extraction.cnmf import cnmf as cnmf
from caiman.motion_correction import MotionCorrect
from caiman.source_extraction.cnmf.utilities import detrend_df_f
from caiman.components_evaluation import estimate_components_quality_auto
import bokeh.plotting as bpl

bpl.output_notebook()


# First, define function for the last iteration of CNMF
# It saves results in the end


def last_masked_cnmf(fname_new, masks, results_index, fname):
    ### BLOCK-1

    # Parameters
    fr = 15.4  # imaging rate in frames per second
    decay_time = 1.0  # 1 second for GCaMP6s, approximate length of a typical transient in seconds
    # motion correction parameters are the same as for mask generation
    niter_rig = 1  # number of iterations for rigid motion correction
    max_shifts = (2, 2)  # maximum allow rigid shift
    splits_rig = (
        75  # for parallelization split the movies in  num_splits chuncks across time
    )
    strides = (
        114,
        114,
    )  # start a new patch for pw-rigid motion correction every x pixels
    overlaps = (30, 30)  # overlap between pathes (size of patch strides+overlaps)
    splits_els = (
        75  # for parallelization split the movies in  num_splits chuncks across time
    )
    upsample_factor_grid = 2  # upsample factor to avoid smearing when merging patches
    max_deviation_rigid = (
        2  # maximum deviation allowed for patch with respect to rigid shifts
    )
    # parameters for source extraction and deconvolution
    p = 1  # order of the autoregressive system, when 0 deconvolution is turned off
    gnb = 2  # number of global background components
    merge_thresh = 0.8  # merging threshold, max correlation allowed
    rf = None  # half-size of the patches in pixels. e.g., if rf=25, patches are 50x50
    stride_cnmf = None  # amount of overlap between the patches in pixels
    K = 80  # it will be taken from the mask
    gSig = [10, 10]  # expected half size of neurons
    init_method = "sparse_nmf"  # initialization method (if analyzing dendritic data using 'sparse_nmf')
    is_dendrites = True  # flag for analyzing dendritic data
    alpha_snmf = 0.8  # it was 0.8, sparsity penalty for dendritic data analysis through sparse NMF
    # parameters for component evaluation

    ### BLOCK-2

    # masks upload from a previous iteration and binarization
    Ain_small_file = np.greater(masks, np.zeros(masks.shape))  # Mask binarization

    # Conversion from matrix to an array
    Ain_small_file = np.ravel(Ain_small_file)
    Ain_small_file = np.reshape(Ain_small_file, (masks.shape))
    Ain_masks = Ain_small_file[:, :]

    print("Ain_masks shape is", Ain_masks.shape)

    # Masks visualization from Ain_masks

    print(Ain_masks.shape)
    print(Ain_masks.dtype)

    ROIs = get_ROI_from_A(Ain_masks, 456, 456)
    print(ROIs.shape)

    # .rcParams['figure.dpi'] = 80
    # .rcParams['figure.figsize'] = (20,12)
    # .rcParams['figure.titlesize'] = 'large'

    #########for i_roi in range(0, ROIs.shape[0]):
    # .subplot(8, 10, i_roi+1)
    # .imshow(ROIs[i_roi, :, :])
    # .title(i_roi,fontsize=10)
    # .axis('off')

    ### BLOCK-3

    # now load the memory mapping file
    Yr, dims, T = cm.load_memmap(fname_new)
    d1, d2 = dims
    images = np.reshape(Yr.T, [T] + list(dims), order="F")
    print("images shape is", images.shape)
    # load frames in python format (T x X x Y)

    # %% restart cluster to clean up memory
    c, dview, n_processes = cm.cluster.setup_cluster(
        backend="local", n_processes=None, single_thread=False
    )

    ### BLOCK-4

    # %% Run CNMF with master mask
    # First extract spatial and temporal components on patches and combine them
    # for this step deconvolution is turned off (p=0)
    t1 = time.time()
    print(n_processes)
    from caiman.source_extraction.cnmf import params as params

    # Set parameteres for mask-based CNMF
    opts_dict = {
        "fnames": fname,
        "fr": fr,
        "decay_time": decay_time,
        "strides": strides,
        "overlaps": overlaps,
        "max_shifts": max_shifts,
        "max_deviation_rigid": max_deviation_rigid,
        "p": 0,
        "gnb": gnb,
        "rf": None,  # rf
        "K": None,  # K
        "gSig": None,  # gSig
        "stride": None,  # stride_cnmf
        "rolling_sum": True,
        "only_init": False,  # True
        "merge_thr": 0.9999,  # merge_thresh    # do not use to 'None', will fail to merge component
    }
    opts = params.CNMFParams(params_dict=opts_dict)
    rf = None  # half-size of the patches in pixels. Should be `None` when seeded CNMF is used.
    only_init = False  # has to be `False` when seeded CNMF is used
    # params object
    opts_dict = {
        "rf": rf,
        "only_init": only_init,
    }
    opts.change_params(opts_dict)
    cnm = cnmf.CNMF(n_processes, params=opts, dview=dview, Ain=Ain_masks)
    try:
        cnm = cnm.fit(images)
    except IndexError:
        p1 = os.path.split(fname_new)[0]
        p2 = "final_masks_" + results_index + "_analysis.npz"
        print(p1)
        print(p2)
        np.savez(os.path.join(p1, p2), A=np.full((1, 1), np.nan))
        return False

    ### BLOCK-5

    print(cnm)
    # %% plot contours of found components
    Cn = cm.local_correlations(images.transpose(1, 2, 0))
    Cn[np.isnan(Cn)] = 0
    # .figure()
    #    crd = plot_contours(cnm.estimates.A, Cn, thr=0.99)
    # .title('Contour plots of found components')

    # %% COMPONENT EVALUATION
    # the components are evaluated in three ways:
    #   a) the shape of each component must be correlated with the data
    #   b) a minimum peak SNR is required over the length of a transient
    min_SNR = 3.0  # signal to noise ratio for accepting a component
    rval_thr = 0.4  # it was 0.4, space correlation threshold for accepting a component
    cnn_thr = 0.4  # threshold for CNN based classifier
    (
        idx_components,
        idx_components_bad,
        SNR_comp,
        r_values,
        cnn_preds,
    ) = estimate_components_quality_auto(
        images,
        cnm.estimates.A,
        cnm.estimates.C,
        cnm.estimates.b,
        cnm.estimates.f,
        cnm.estimates.YrA,
        fr,
        decay_time,
        gSig,
        dims,
        dview=dview,
        min_SNR=min_SNR,
        r_values_min=rval_thr,
        use_cnn=False,
        thresh_cnn_lowest=cnn_thr,
    )

    ### BLOCK-6

    # %% RE-RUN seeded CNMF on accepted patches to refine and perform deconvolution
    A_in = cnm.estimates.A[:, idx_components]
    C_in = cnm.estimates.C[idx_components]
    b_in = cnm.estimates.b
    f_in = cnm.estimates.f
    cnm2 = cnmf.CNMF(
        n_processes=n_processes,
        k=A_in.shape[-1],
        gSig=gSig,
        p=1,  # It is here for deconvolution
        dview=dview,
        merge_thresh=0.999,  # merge_thresh,
        Ain=A_in,
        Cin=C_in,
        b_in=b_in,
        f_in=f_in,
        rf=None,
        stride=None,
        gnb=gnb,
        method_deconvolution="oasis",
        check_nan=True,
    )
    cnm2 = cnm2.fit(images)

    # %% Extract DF/F values
    F_dff = detrend_df_f(
        cnm2.estimates.A,
        cnm2.estimates.b,
        cnm2.estimates.C,
        cnm2.estimates.f,
        YrA=cnm2.estimates.YrA,
        quantileMin=8,
        frames_window=250,
    )

    ### BLOCK-7

    # Results saving from CNM2
    p1 = os.path.split(fname_new)[0]
    p2 = "final_masks_" + results_index + "_analysis.npz"
    print(p1)
    print(p2)
    np.savez(
        os.path.join(p1, p2),
        S=cnm2.estimates.S,  # added 12.9.2019
        F_dff=F_dff,  # added 12.9.2019
        Cn=Cn,
        A=cnm2.estimates.A.todense(),  # these are spatial patches
        C=cnm2.estimates.C,
        b=cnm2.estimates.b,
        f=cnm2.estimates.f,
        YrA=cnm2.estimates.YrA,
        sn=1,
        d1=d1,
        d2=d2,
        idx_components=idx_components,
        idx_components_bad=idx_components_bad,
    )

    ### BLOCK-8

    # %% STOP CLUSTER and clean up log files
    cm.stop_server(dview=dview)
    log_files = glob.glob("*_LOG_*")
    for log_file in log_files:
        os.remove(log_file)
    return True


def intermediate_masked_cnmf(masks, fname_new, fname):
    ### BLOCK-1

    # Parameters
    fr = 15.4  # imaging rate in frames per second
    decay_time = 1.0  # 1 second for GCaMP6s, approximate length of a typical transient in seconds
    # motion correction parameters are the same as for mask generation
    niter_rig = 1  # number of iterations for rigid motion correction
    max_shifts = (2, 2)  # maximum allow rigid shift
    splits_rig = (
        75  # for parallelization split the movies in  num_splits chuncks across time
    )
    strides = (
        114,
        114,
    )  # start a new patch for pw-rigid motion correction every x pixels
    overlaps = (30, 30)  # overlap between pathes (size of patch strides+overlaps)
    splits_els = (
        75  # for parallelization split the movies in  num_splits chuncks across time
    )
    upsample_factor_grid = 2  # upsample factor to avoid smearing when merging patches
    max_deviation_rigid = (
        2  # maximum deviation allowed for patch with respect to rigid shifts
    )
    # parameters for source extraction and deconvolution
    p = 1  # order of the autoregressive system, when 0 deconvolution is turned off
    gnb = 2  # number of global background components
    merge_thresh = 0.8  # merging threshold, max correlation allowed
    rf = None  # half-size of the patches in pixels. e.g., if rf=25, patches are 50x50
    stride_cnmf = None  # amount of overlap between the patches in pixels
    K = 80  # it will be taken from the mask
    gSig = [10, 10]  # expected half size of neurons
    init_method = "sparse_nmf"  # initialization method (if analyzing dendritic data using 'sparse_nmf')
    is_dendrites = True  # flag for analyzing dendritic data
    alpha_snmf = 0.8  # it was 0.8, sparsity penalty for dendritic data analysis through sparse NMF
    # parameters for component evaluation

    c, dview, n_processes = cm.cluster.setup_cluster(
        backend="local", n_processes=None, single_thread=False
    )

    ### BLOCK-2

    # masks upload from a previous iteration and binarization
    Ain_small_file = np.greater(masks, np.zeros(masks.shape))  # Mask binarization

    # Conversion from matrix to an array
    Ain_small_file = np.ravel(Ain_small_file)
    Ain_small_file = np.reshape(Ain_small_file, (masks.shape))
    Ain_masks = Ain_small_file[:, :]

    print("Ain_masks shape is", Ain_masks.shape)

    # Masks visualization from Ain_masks

    print(Ain_masks.shape)
    print(Ain_masks.dtype)

    ROIs = get_ROI_from_A(Ain_masks, 456, 456)
    print(ROIs.shape)

    # .rcParams['figure.dpi'] = 80
    # .rcParams['figure.figsize'] = (20,12)
    # .rcParams['figure.titlesize'] = 'large'

    # for i_roi in range(0, ROIs.shape[0]):
    # .subplot(8, 10, i_roi+1)
    # .imshow(ROIs[i_roi, :, :])
    # .title(i_roi,fontsize=10)
    # .axis('off')

    ### BLOCK-3

    # now load the memory mapping file
    Yr, dims, T = cm.load_memmap(fname_new)
    d1, d2 = dims
    images = np.reshape(Yr.T, [T] + list(dims), order="F")
    print("images shape is", images.shape)
    # load frames in python format (T x X x Y)

    # %% restart cluster to clean up memory
    #    cm.stop_server(dview=dview)
    dview.terminate()
    c, dview, n_processes = cm.cluster.setup_cluster(
        backend="local", n_processes=None, single_thread=False
    )

    ### BLOCK-4

    # %% Run CNMF with master mask
    # First extract spatial and temporal components on patches and combine them
    # for this step deconvolution is turned off (p=0)
    t1 = time.time()
    print(n_processes)
    from caiman.source_extraction.cnmf import params as params

    # Set parameteres for mask-based CNMF
    opts_dict = {
        "fnames": fname,
        "fr": fr,
        "decay_time": decay_time,
        "strides": strides,
        "overlaps": overlaps,
        "max_shifts": max_shifts,
        "max_deviation_rigid": max_deviation_rigid,
        "p": 0,
        "gnb": gnb,
        "rf": None,  # rf
        "K": None,  # K
        "gSig": None,  # gSig
        "stride": None,  # stride_cnmf
        "rolling_sum": True,
        "only_init": False,  # True
        "merge_thr": 0.9999,  # merge_thresh    # do not use to 'None', will fail to merge component
    }
    opts = params.CNMFParams(params_dict=opts_dict)
    rf = None  # half-size of the patches in pixels. Should be `None` when seeded CNMF is used.
    only_init = False  # has to be `False` when seeded CNMF is used
    # params object
    opts_dict = {
        "rf": rf,
        "only_init": only_init,
    }
    opts.change_params(opts_dict)
    cnm = cnmf.CNMF(n_processes, params=opts, dview=dview, Ain=Ain_masks)
    cnm = cnm.fit(images)

    ### BLOCK-5

    print(cnm)
    # %% plot contours of found components
    Cn = cm.local_correlations(images.transpose(1, 2, 0))
    Cn[np.isnan(Cn)] = 0
    # .figure()
    #    crd = plot_contours(cnm.estimates.A, Cn, thr=0.99)
    # .title('Contour plots of found components')

    # %% COMPONENT EVALUATION
    # the components are evaluated in three ways:
    #   a) the shape of each component must be correlated with the data
    #   b) a minimum peak SNR is required over the length of a transient
    min_SNR = 3.0  # signal to noise ratio for accepting a component
    rval_thr = 0.4  # it was 0.4, space correlation threshold for accepting a component
    cnn_thr = 0.4  # threshold for CNN based classifier
    (
        idx_components,
        idx_components_bad,
        SNR_comp,
        r_values,
        cnn_preds,
    ) = estimate_components_quality_auto(
        images,
        cnm.estimates.A,
        cnm.estimates.C,
        cnm.estimates.b,
        cnm.estimates.f,
        cnm.estimates.YrA,
        fr,
        decay_time,
        gSig,
        dims,
        dview=dview,
        min_SNR=min_SNR,
        r_values_min=rval_thr,
        use_cnn=False,
        thresh_cnn_lowest=cnn_thr,
    )

    # Good and bad components

    #    crd_good = cm.utils.visualization.plot_contours(cnm.estimates.A[:,idx_components], Cn, thr=.8, vmax=0.75)
    #    crd_bad = cm.utils.visualization.plot_contours(cnm.estimates.A[:,idx_components_bad], Cn, thr=.8, vmax=0.75)

    ### BLOCK-6

    # %% RE-RUN seeded CNMF on accepted patches to refine and perform deconvolution
    A_in = cnm.estimates.A[:, idx_components]
    C_in = cnm.estimates.C[idx_components]
    b_in = cnm.estimates.b
    f_in = cnm.estimates.f
    cnm2 = cnmf.CNMF(
        n_processes=n_processes,
        k=A_in.shape[-1],
        gSig=gSig,
        p=1,  # It is here for deconvolution
        dview=dview,
        merge_thresh=0.999,  # merge_thresh,
        Ain=A_in,
        Cin=C_in,
        b_in=b_in,
        f_in=f_in,
        rf=None,
        stride=None,
        gnb=gnb,
        method_deconvolution="oasis",
        check_nan=True,
    )
    cnm2 = cnm2.fit(images)

    # %% Extract DF/F values
    F_dff = detrend_df_f(
        cnm2.estimates.A,
        cnm2.estimates.b,
        cnm2.estimates.C,
        cnm2.estimates.f,
        YrA=cnm2.estimates.YrA,
        quantileMin=8,
        frames_window=250,
    )

    ### BLOCK-7

    masks = cnm2.estimates.A.todense()  #  Here we are getting masks

    # STOP CLUSTER and clean up log files
    cm.stop_server(dview=dview)
    log_files = glob.glob("*_LOG_*")
    for log_file in log_files:
        os.remove(log_file)

    return masks


# Initial masks generation and memory mapping


def initialize_masks(fname, K):
    ### BLOCK-1
    # Spatial masks generation at 456x456 px and GCaMP6s
    # dataset dependent parameters

    fr = 15.4  # imaging rate in frames per second
    decay_time = 1.0  # it was 0.4, length of a typical transient in seconds

    # motion correction parameters
    niter_rig = 1  # (it was 3) number of iterations for rigid motion correction
    max_shifts = (2, 2)  # (it was 7, 7) maximum allow rigid shift
    splits_rig = 75  # (it was 56) for parallelization split the movies in  num_splits chuncks across time
    strides = (
        114,
        114,
    )  # (it was 48) start a new patch for pw-rigid motion correction every x pixels
    overlaps = (
        30,
        30,
    )  # (it was 24) overlap between pathes (size of patch strides+overlaps)
    splits_els = 75  # (it was 56) for parallelization split the movies in  num_splits chuncks across time
    upsample_factor_grid = 2  # upsample factor to avoid smearing when merging patches
    max_deviation_rigid = (
        2  # (it was 3) maximum deviation allowed for patch with respect to rigid shifts
    )

    # pw_rigid = False  # flag for performing rigid or piecewise rigid motion correction

    # parameters for source extraction and deconvolution
    p = 1  # order of the autoregressive system, when 0 deconvolution is turned off
    gnb = 2  # number of global background components
    merge_thresh = 0.95  # was 0.8, merging threshold, max correlation allowed
    rf = None  # half-size of the patches in pixels. e.g., if rf=25, patches are 50x50
    stride_cnmf = None  # amount of overlap between the patches in pixels
    K = K  # Number of components per patch
    gSig = [20, 20]  # expected half size of neurons
    init_method = "sparse_nmf"  # initialization method (if analyzing dendritic data using 'sparse_nmf')
    is_dendrites = True  # flag for analyzing dendritic data
    alpha_snmf = 0.8  # it was 0.8, sparsity penalty for dendritic data analysis through sparse NMF

    ### BLOCK-2
    # %% start a cluster for parallel processing
    # dview.terminate()
    c, dview, n_processes = cm.cluster.setup_cluster(
        backend="local", n_processes=None, single_thread=False
    )

    # first we create a motion correction object with the parameters specified
    min_mov = min(
        cm.load(fname[:], subindices=range(200)).min(), 0
    )  # cm.load(fname[0],
    # this will be subtracted from the movie to make it non-negative

    mc = MotionCorrect(
        fname[:],  # mc = MotionCorrect(fname[0],
        min_mov,
        dview=dview,
        max_shifts=max_shifts,
        niter_rig=niter_rig,
        splits_rig=splits_rig,
        strides=strides,
        overlaps=overlaps,
        splits_els=splits_els,
        upsample_factor_grid=upsample_factor_grid,
        max_deviation_rigid=max_deviation_rigid,
        shifts_opencv=True,
        nonneg_movie=True
        # , pw_rigid=pw_rigid
    )
    # note that the file is not loaded in memory

    # %%capture
    # %% Run piecewise-rigid motion correction using NoRMCorre
    mc.motion_correct_pwrigid(save_movie=True)  # Perform motion correction
    print(mc.fname_tot_els)  # Print list of MMAP file(s)
    m_els = cm.load(mc.fname_tot_els)  # Load MMAP file(s) to memory
    bord_px_els = np.ceil(
        np.maximum(np.max(np.abs(mc.x_shifts_els)), np.max(np.abs(mc.y_shifts_els)))
    ).astype(np.int)
    # maximum shift to be used for trimming against NaNs

    ### BLOCK-3
    # %% MEMORY MAPPING
    # memory map the file in order 'C'
    fnames = mc.fname_tot_els  # name of the pw-rigidly corrected file.
    border_to_0 = bord_px_els  # number of pixels to exclude
    fname_new = cm.save_memmap(
        fnames, base_name="memmap_", order="C", border_to_0=bord_px_els
    )  # exclude borders

    # now load the file
    Yr, dims, T = cm.load_memmap(fname_new)
    d1, d2 = dims
    images = np.reshape(Yr.T, [T] + list(dims), order="F")
    # load frames in python format (T x X x Y)

    # %% restart cluster to clean up memory
    # cm.stop_server(dview=dview)
    dview.terminate()
    c, dview, n_processes = cm.cluster.setup_cluster(
        backend="local", n_processes=None, single_thread=False
    )

    ### BLOCK-4
    # %%capture
    # %% RUN CNMF ON PATCHES

    # First extract spatial and temporal components on patches and combine them
    # for this step deconvolution is turned off (p=0)

    t1 = time.time()
    print(n_processes)
    cnm = cnmf.CNMF(
        n_processes=n_processes,
        k=K,
        gSig=gSig,
        merge_thresh=merge_thresh,
        p=0,
        dview=dview,
        rf=rf,
        stride=stride_cnmf,
        memory_fact=4,  # 1, default 16 GB
        method_init=init_method,
        alpha_snmf=alpha_snmf,
        only_init_patch=False,
        gnb=gnb,
        border_pix=bord_px_els  # ,
        # n_pixels_per_process = None, #4000, # default
        # block_size_temp = 490, #5000,  # default
        # block_size_spat = 10000, #5000, # default
        # num_blocks_per_run_spat = 40, #20, # default
        # num_blocks_per_run_temp = 10 #20 # default
    )

    cnm = cnm.fit(images)

    print(cnm)
    # %% plot contours of found components
    Cn = cm.local_correlations(images.transpose(1, 2, 0))
    Cn[np.isnan(Cn)] = 0

    ### BLOCK-5
    # parameters for component evaluation
    min_SNR = 3.0  # signal to noise ratio for accepting a component
    rval_thr = 0.4  # it was 0.8, space correlation threshold for accepting a component
    cnn_thr = 0.4  # threshold for CNN based classifier

    # %% COMPONENT EVALUATION
    # the components are evaluated in three ways:
    #   a) the shape of each component must be correlated with the data
    #   b) a minimum peak SNR is required over the length of a transient

    (
        idx_components,
        idx_components_bad,
        SNR_comp,
        r_values,
        cnn_preds,
    ) = estimate_components_quality_auto(
        images,
        cnm.estimates.A,
        cnm.estimates.C,
        cnm.estimates.b,
        cnm.estimates.f,
        cnm.estimates.YrA,
        fr,
        decay_time,
        gSig,
        dims,
        dview=dview,
        min_SNR=min_SNR,
        r_values_min=rval_thr,
        use_cnn=False,
        thresh_cnn_lowest=cnn_thr,
    )

    #    crd_good = cm.utils.visualization.plot_contours(cnm.estimates.A[:,idx_components], Cn, thr=.8, vmax=0.75)
    #    crd_bad = cm.utils.visualization.plot_contours(cnm.estimates.A[:,idx_components_bad], Cn, thr=.8, vmax=0.75)

    ### BLOCK-6
    # %%capture
    # %% RE-RUN seeded CNMF on accepted patches to refine and perform deconvolution
    A_in = cnm.estimates.A[:, idx_components]
    C_in = cnm.estimates.C[idx_components]
    b_in = cnm.estimates.b
    f_in = cnm.estimates.f

    cnm2 = cnmf.CNMF(
        n_processes=n_processes,
        k=A_in.shape[-1],
        gSig=gSig,
        p=1,
        dview=dview,
        merge_thresh=0.999,  # merge_thresh,
        Ain=A_in,
        Cin=C_in,
        b_in=b_in,
        f_in=f_in,
        rf=None,
        stride=None,
        gnb=gnb,
        method_deconvolution="oasis",
        check_nan=True,
    )

    cnm2 = cnm2.fit(images)

    # %% Extract DF/F values
    F_dff = detrend_df_f(
        cnm2.estimates.A,
        cnm2.estimates.b,
        cnm2.estimates.C,
        cnm2.estimates.f,
        YrA=cnm2.estimates.YrA,
        quantileMin=8,
        frames_window=250,
    )

    ### BLOCK-7

    masks = cnm2.estimates.A.todense()  #  Here we are getting masks

    # %% STOP CLUSTER and clean up log files
    cm.stop_server(dview=dview)
    log_files = glob.glob("*_LOG_*")
    for log_file in log_files:
        os.remove(log_file)

    return masks, fname_new


# %% Get complete file names with the path from pointed folder
from os import listdir


def get_files(dirpath):
    filelist = listdir(dirpath)
    fname = []
    for i in range(len(filelist)):
        filename = filelist[i]
        filepath = dirpath + "/" + filename
        fname.append(filepath)
    return fname


# function for Ain reshape before visualization


def get_ROI_from_A(A, frame_H, frame_W):
    from numpy import reshape
    import scipy

    A_arr = A
    A_arr_reshape = reshape(A_arr, (frame_W, frame_H, A.shape[1])).T
    return A_arr_reshape
