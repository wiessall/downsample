from DM_masked_cnmf_lib import *
import argparse
import os
import warnings

warnings.filterwarnings("ignore")


parser = argparse.ArgumentParser()
parser.add_argument(
    "-i",
    "--input",
    type=str,
    default="to_ana",
    help="input path cotaining motion-corrected tifs",
)
parser.add_argument(
    "-a",
    "--amat",
    nargs="+",
    type=str,
    default="to_ana",
    help="input path cotaining A matrix",
)
args = parser.parse_args()

# Second, run the function for multiple folders and files


#### OptoCtrl2 FF

# Path to folder to be analyzed (all files and subfolders will be picked)
dirpath = args.input
list_of_files = sorted(glob.glob(os.path.join(dirpath, "*tif")))

print(args.amat)
print(list_of_files)
for mask_folder in args.amat:
    # Path to masks. There will be no selection
    path_to_masks = os.path.join(mask_folder, "A.npy")
    print(path_to_masks)
    # Index for the name of the result file
    results_index = "{}_{}".format(mask_folder.split("/")[-1], "full_cnmf")

    # pipeline launch
    exitCode = masked_cnmf(list_of_files, path_to_masks, results_index)
    if exitCode is False:
        print("**** {} FAILED ****".format(mask_folder))
    print("**** CNMF finished ****")
