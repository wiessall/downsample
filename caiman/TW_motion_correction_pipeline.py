""" Perform a motion correction in caiman, intended for somata recordings, based on Leiron's procedure. Should be obsolete if motion correction with suite2p was done. """

import glob
import logging
import numpy as np
import os
import yaml
from natsort import natsorted
import argparse 
from tifffile import imsave, imwrite, imread, TiffFile

from DM_motion_correction_lib import format_suite2p_normal, split_stack
from utils import set_caiman_parameters


import caiman as cm
from caiman.motion_correction import MotionCorrect
from caiman.source_extraction.cnmf import params as params


def main(args) -> None:
    """Run caiman motion correction using provided commandline arguments."""

    # dataset dependent parameters
    wdir = args.input
    save_dir = args.output
    fname = natsorted(glob.glob(os.path.join(wdir, "*tif")))

    # The files generated here must not be shorter than 200 frames, caimans movie
    # is expecting longer files
    for number, f in enumerate(fname):
        format_suite2p_normal(f)
        if len(TiffFile(f).pages) < 499:
            current_tif = imread(f)
            os.remove(f)
            last_f = fname[number - 1]
            last_tif = imread(last_f)
            current_tif = np.concatenate([last_tif, current_tif])
            imwrite(last_f, current_tif)

    fname = natsorted(glob.glob(os.path.join(wdir, "*tif")))
    assert len(fname) > 0, f"{wdir} does not contain any .tif files to load!"
    print(f"ALL FILES: {fname}")

    # start a cluster for parallel processing
    c, dview, n_processes = cm.cluster.setup_cluster(
        backend="local", n_processes=None, single_thread=False
    )

    # first we create a motion correction object with the parameters specified
    min_mov = min(
        cm.load_movie_chain(fname, subindices=range(200)).min(), 0
    ) 
    # this will be subtracted from the movie to make it non-negative

    caiman_opts_dict = set_caiman_parameters(args.parameters)
    min_mov_dict = {"min_mov": min_mov}
      
    opts_mc = params.CNMFParams(params_dict=caiman_opts_dict)
    opts_mc.change_params(min_mov_dict)

    mc = MotionCorrect(fname, dview=dview, **opts_mc.get_group("motion"))
    # %% Run piecewise-rigid motion correction using NoRMCorre
    mc.motion_correct(save_movie=True)  # Perform motion correction
    print(mc.fname_tot_els)  # Print list of MMAP file(s)
    m_els = cm.load(mc.fname_tot_els)  # Load MMAP file(s) to memory

    # Crop to expected 456 x 456 px shape
    m_els = m_els[:, 28:-28, 28:-28].astype(np.int16)

    print("Saving *.memmap")

    # MEMORY MAPPING
    # This pipeline loads memmaps, tifs are redundant below
    # memory map the file in order 'C'
    border_to_0 = 0 if mc.border_nan == 'copy' else mc.border_to_0

    fname_new = cm.save_memmap(mc.mmap_file,
        base_name = os.path.join(save_dir, "_NormCorre_MC"),
        order = 'C',
        border_to_0 = border_to_0,
        dview = dview
    ) # exclude borders
    print("fname_new")


    ####STACK SPLIT
    m_els = split_stack(m_els)
    m_els = [element for element in m_els if element.shape[0]]
    for i, substack in enumerate(m_els):
        savepath = os.path.join(save_dir, str(i) + "_NormCorre_MC.tif")
        print(savepath)
        imsave(savepath, substack, bigtiff=False)

    # %% STOP CLUSTER and clean up log files
    cm.stop_server(dview=dview)
    log_files = glob.glob("*_LOG_*")
    for log_file in log_files:
        os.remove(log_file)

    dview.terminate()
    
    return fname_new

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # set start and end time
    parser.add_argument(
        "-i",
        "--input",
        type=str,
        default=".",
        help="input folder, takes tif files from there",
    )

    parser.add_argument(
        "-o", "--output", type=str, default="to_ana", help="output folder"
    )
    
    parser.add_argument(
      "-p", "--parameters", type=str,
      help="caiman parameters (YAML file)"
    )
    args = parser.parse_args()

    os.makedirs(args.output, exist_ok=True)
    
    main(args)
