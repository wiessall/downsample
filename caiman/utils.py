import yaml


def set_caiman_parameters(param_file: str) -> dict:
    '''
    Read the yaml file for caiman parameters and parse it as a dictionary

    Parameters
    ----------
    param_file : str
        DESCRIPTION.

    Returns
    -------
    param_dict
        DESCRIPTION.

    '''
    """Set the matplotlib parameters."""
    with open(param_file, "r") as reader:
        param_dict = yaml.safe_load(reader)

    return param_dict

