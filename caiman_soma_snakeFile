#! /g/easybuild/x86_64/Rocky/8/haswell/software/Python/3.9.6-GCCcore-11.2.0/bin/python

localrules: cleanup
containerized: "docker://registry.git.embl.de/wiessall/downsample/environment:a8847fc4"

rule all:
    input:
        expand(config["base_dir"] + "{sample}/001_THIS_FOLDER_IS_PROCESSED_{sample}", sample=config["sample_dirs"], number=range(1, 9))


rule raw1024tiff512:
    output:
        config["base_dir"] + "{sample}/tifs/" + "done",
    params:
        basedir=config["base_dir"],
    log:
        "logs/raw1024tif512_{sample}.log"
    conda:
        "envs/raw2tiff.yaml"
    shell:
        """
        python3 raw2tiff/downsample.py -i {params.basedir}/{wildcards.sample} -o {params.basedir}/{wildcards.sample}/tifs && touch {params.basedir}/{wildcards.sample}/tifs/done
        """


rule suite2pMC:
    input:
        config["base_dir"] + "{sample}/tifs/" + "done",
    output:
        config["base_dir"] + "{sample}_suite2p_done",
        directory(config["base_dir"] + "{sample}/tifs/suite2p/plane0/reg_tif/"),
    params:
        uri=config["suite2pMC-uri"],
        basedir=config["base_dir"],
    log:
        "logs/suite2pMC_{sample}.log"
    conda:
        "envs/suite2p.yaml"
    shell:
        """
        python suite2p/run_all.py -i {params.basedir}{wildcards.sample}/tifs -o {params.basedir}{wildcards.sample}/tifs && \
        touch {output}
        """


rule caimanMC:
    input:
        config["base_dir"] + "{sample}_suite2p_done",
        config["base_dir"] + "{sample}/tifs/suite2p/plane0/reg_tif/",
    output:
        expand(config["base_dir"] + "{{sample}}/to_ana/{number}_NormCorre_MC.tif", number=range(1, 9)),
    params:
        caiman_params=config["caiman_params"],
        basedir=config["base_dir"],
    log:
        "logs/caimanMC_{sample}.log"
    conda:
        "envs/caiman.yaml"
    shell:
        """
        python3 caiman/TW_motion_correction_pipeline.py -i {params.basedir}{wildcards.sample}/tifs/suite2p/plane0/reg_tif/ -o {params.basedir}{wildcards.sample}/to_ana/ -p {params.caiman_params}
        """


rule caimanFullCNMF:
    input:
        expand(config["base_dir"] + "{{sample}}/to_ana/{number}_NormCorre_MC.tif", number=range(1, 9)),
    output:
        expand(config["base_dir"] + "{{sample}}/to_ana" + "/final_masks_analysis_full_cnmf.npz")
    params:
        mem_dir=expand(config["base_dir"] + "{{sample}}/to_ana/"),
        caiman_params=config["caiman_params"],
    log:
        "logs/caimanFullCNMF_{sample}.log"
    conda:
        "envs/caiman.yaml"
    shell:
        """
            python3 caiman/TW_full_cnmf_pipeline.py -f {params.mem_dir} -p {params.caiman_params}
        """

rule cleanup:
    input:
        expand(config["base_dir"] + "{{sample}}/to_ana" + "/final_masks_analysis_full_cnmf.npz", kSet=config["k_estimates"])
    output:
        expand(config["base_dir"] + "{{sample}}/001_THIS_FOLDER_IS_PROCESSED_{{sample}}")
    params:
        baseDir=config["base_dir"]
    log:
        "logs/cleanup_{sample}.log"
    shell:
        """
            find {params.baseDir}/{wildcards.sample} -iname "*mmap" | xargs rm -f && \
            touch {output}
        """
