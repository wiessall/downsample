#
# Configuration file for using the XML library in GNOME applications
#
XML2_LIBDIR="-L/home/ferrares/.conda/envs/nn/lib"
XML2_LIBS="-lxml2 -L/home/ferrares/.conda/envs/nn/lib -lz -L/home/ferrares/.conda/envs/nn/lib -llzma  -L/home/ferrares/.conda/envs/nn/lib -L/home/ferrares/.conda/envs/nn/lib -licui18n -licuuc -licudata -lm "
XML2_INCLUDEDIR="-I/home/ferrares/.conda/envs/nn/include/libxml2 -I/home/ferrares/.conda/envs/nn/include"
MODULE_VERSION="xml2-2.9.8"

