#!/usr/bin/bash
#SBATCH --output='/g/asari/Tristan/slurm-logs/snakemake_%a_%j.err'
#SBATCH --error='/g/asari/Tristan/slurm-logs/snakemake_%a_%j.out'
#SBATCH --job-name=SNKMK
#SBATCH --mail-type=FAIL,BEGIN,END
#SBATCH --mail-user=tristan.wiessalla@embl.it
#SBATCH -p "htc-el8"
#SBATCH -t 48:00:00


source ~/.bashrc
module purge
module load Anaconda3/2022.05
eval "$(conda shell.bash hook)" 
conda activate snakemake-7.20.0 && \
snakemake --version && \
snakemake -p --configfile ./snakemake/config.yaml --profile wiessall -j 10 --snakefile caiman_axons_snakeFile
