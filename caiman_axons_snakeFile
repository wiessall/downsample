#! /g/easybuild/x86_64/Rocky/8/haswell/software/Python/3.9.6-GCCcore-11.2.0/bin/python
localrules: unzip, cleanup

rule all:
    input:
            expand(config["base_dir"] + "{sample}/001_THIS_FOLDER_IS_PROCESSED_{sample}", sample=config["sample_dirs"], number=range(1,9))

#rule raw1024tiff512:
#    input: 
#            config["base_dir"] + "{sample}",
#    output:
#            config["base_dir"] + "{sample}/tifs/" + "done",
#    container: 
#            config["raw1024tiff512-uri"]
#    log:
#            "logs/raw1024tif512_{sample}.log"
#    shell:
#            """
#                python3 /src/raw2tiff/downsample.py -i /px/{wildcards.sample} -o /px/{wildcards.sample}/tifs && touch /px/{wildcards.sample}/tifs/done
#            """
#
#rule suite2pMC:
#    input: 
#            config["base_dir"] + "{sample}/tifs/" + "done",
#    output:
#            directory(config["base_dir"] + "{sample}/tifs/suite2p/plane0/reg_tif/"),
#    params: 
#        uri="/home/wiessall/.singularity/cache/oci-tmp/fe0c8c2e38789b65efe89d7b7fe9ea368bfa623e9b280857716981e20fde6fcc",
#        bind=config["base_dir"], 
#    log:
#            "logs/suite2pMC_{sample}.log"
#    shell:
#            """
#               unset XALT_EXECUTABLE_TRACKING &&\
#                       singularity exec --bind /g/asari/Tristan/Software/downsample:/src --bind {params.bind}:/px {params.uri} /opt/conda/envs/scipy-dev/bin/python3 /src/suite2p/run_all.py -i /px/{wildcards.sample}/tifs -o /px/{wildcards.sample}/tifs
#            """
#
#rule caimanMC:
#    input: 
#            config["base_dir"] + "{sample}/tifs/suite2p/plane0/reg_tif/",
#    output:
#            expand(config["base_dir"] + "{{sample}}/to_ana/{number}_NormCorre_MC.tif", number=range(1,9)),
#    container: 
#            config["caiman-uri"]
#    log:
#            "logs/caimanMC_{sample}.log"
#    shell:
#            """
#                python3 /src/caiman/DM_motion_correction_pipeline.py -i /px/{wildcards.sample}/tifs/suite2p/plane0/reg_tif/ -o /px/{wildcards.sample}/to_ana/
#            """
#
rule caimanIterativeMaskGeneration:
    input: 
            expand(config["base_dir"] + "{{sample}}/to_ana/{number}_NormCorre_MC.tif", number=range(1,9)),
    output: 
            expand(config["base_dir"] + "{{sample}}/to_ana/" + "final_masks_K{kSet}_analysis.npz", kSet=config["k_estimates"]),
    params:
            kSet=config["k_estimates"],
    container: 
            config["caiman-uri"]
    log:
            "logs/caimanIterativeMaskGeneration_{sample}.log"
    shell:
            """
                python3 /src/caiman/DM_iterative_mask_generation_pipeline.py -i /px/{wildcards.sample}/to_ana/ -k {params.kSet}
            """

rule unzip:
    input:
            expand(config["base_dir"] + "{{sample}}/to_ana/" + "final_masks_K{kSet}_analysis.npz", kSet=config["k_estimates"]),
    output:                 
            directory(expand(config["base_dir"] + "{{sample}}/to_ana/" +"final_masks_K{kSet}_analysis", kSet=config["k_estimates"]))
    log:
            "logs/unzip_{sample}.log"
    shell:
            """
                /bin/bash unzip_d.sh {input}
            """

rule caimanFullCNMF:
    input: 
            expand(config["base_dir"] + "{{sample}}/to_ana/" +"final_masks_K{kSet}_analysis", kSet=config["k_estimates"])
    output:
            expand(config["base_dir"] + "{{sample}}/to_ana" + "/final_masks_K{kSet}_analysis_full_cnmf.npz", kSet=config["k_estimates"])
    params:
            tifDir=config["out_caiman_dir"],
            aDir=expand("/px/{{sample}}/to_ana/" +"final_masks_K{kSet}_analysis", kSet=config["k_estimates"])
    container: 
            config["caiman-uri"]
    log:
            "logs/caimanFullCNMF_{sample}.log"
    shell:
            """
                python3 /src/caiman/DM_masked_cnmf_pipeline.py -i /px/{wildcards.sample}/{params.tifDir} -a {params.aDir}
            """
rule cleanup:
    input:
            expand(config["base_dir"] + "{{sample}}/to_ana" + "/final_masks_K{kSet}_analysis_full_cnmf.npz", kSet=config["k_estimates"])
    output:
            expand(config["base_dir"] + "{{sample}}/001_THIS_FOLDER_IS_PROCESSED_{{sample}}")
    params:
            baseDir=config["base_dir"]
    log:
            "logs/cleanup_{sample}.log"
    shell:
        """
            find {params.baseDir}/{wildcards.sample} -iname "*mmap" | xargs rm -f && \
            touch {output}
        """

