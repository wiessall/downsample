import os 
import glob 
from pathlib import Path 

import argparse
import numpy as np
import cv2

from tifffile import imread, imsave
from utils import FileType_Base, FileType_RAW, Load_Monochrome_RAW


parser = argparse.ArgumentParser()
# set start and end time
parser.add_argument('-i', '--input', type=str, default=".",
                 help="input folder, takes raw files from there")
parser.add_argument('-o', '--output', type=str, default=".",
                 help="output folder")

args = parser.parse_args()

files = sorted(glob.glob(os.path.join(args.input, '*.raw')))
files = [Path(path) for path in files]
assert len(files) > 0, "no files in specified folder {}".format(args.input)


output = args.output
os.makedirs(output, exist_ok=True)

for filename in files:
        basename = filename.stem
        filename = str(filename)
        stack, _ = Load_Monochrome_RAW(
                        file_name           = filename,
                        frame_width         = 1024,
                        frame_height        = 1024,
                        bits_per_pixel      = 16,
                        channels            = 1,
                        frames_limit        = -1,
                        frames_to_skip      = 0,
                        debug               = None)

        stack = stack.squeeze()
        print(stack.shape)
        target_arr = np.empty((stack.shape[0], stack.shape[1] // 2, stack.shape[2] // 2), dtype=np.int16)

        for idx, im in enumerate(stack):


            result = cv2.pyrDown(im)
            target_arr[idx,...] = result

        savename = basename + ".tif"


        output = os.path.join(args.output, savename)

        print(output)

        imsave(output, target_arr)
