#!/usr/bin/bash

#conda activate Eric

snakemake -p --configfile "$1" \
--profile Slurm \
-j 10 \
--snakefile methSnakeFile
